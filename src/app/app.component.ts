import {Component, OnInit} from '@angular/core';
import {Pipe, PipeTransform} from '@angular/core';
import {Http} from '@angular/http';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  title = 'app';

  constructor(private http: Http) {

  }

  people: any [];    // define people


  ngOnInit() {                                          // loading JSON data
    this.http.get('../assets/people.json')
      .subscribe(res => {
        this.people = res.json();
        console.log('this.people', this.people);


        const num = 0;
        let count = 0;
         this.mappedArray = [];


        // used foreach to change date format
        this.people.forEach(function (item) {
          item['first_name'] = item ['user'] ['first_name'];
          item['data']['TIME'][0]['START'] = new Date(item['data']['TIME'][0]['START']).toString().slice(0,15);;

     });


        this.people.map(item => {
          return {
            userName: item['user']['first_name'],
            createdOn: item['data']['TIME'][0]['START']
          };
        }).forEach(item => this.mappedArray.push(item));

        console.log('mappedArray', this.mappedArray);


      });
  }

        remove(person): void {

          console.log('user', person);
          const index: number = this.people.indexOf(person);
          if (index !== -1) {
            this.people.splice(index, 1);
          }

       });



        add(): void{
          this.people.push({"time_taken":0, "user_id":999, "user":{ "tags":[ "UI Development" ], "img":"https://lh3.googleusercontent.com7rxVanc/photo.jpg?sz=50", "id":50.0, "first_name":"dummy", "last_name":"" }, "date":"2018-02-28T00:00:00+00:00", "description":"Updated Existing cards for next week", "activity":{ "project":{ "client":{ "name":"Pepper Square", "img":"https://gtd.s3.amazonaws.com/client/psq_logo.png", "id":1.0, "archived":false, "data":null }, "img":null, "status":null, "data":{ "admin_group":[ ], "planned_hours":120, "acl":[ 11, 1, 6, 4, 12, 15, 13, 22, 20, 41, 53, 54, 60, 5, 32, 63, 67, 65, 68, 66, 58, 69, 71, 72, 74, 75, 78, 79, 50 ] }, "name":"Internal", "link":null, "client_id":1, "due":"2018-01-23T18:30:00+00:00", "id":52.0, "archived":false }, "title":"Checking trello and GTD", "id":22444.0, "due_on":"2018-02-27T18:30:00+00:00" }, "data":{ "TIME":[ { "STOP":"2018-02-28T04:27:48Z", "START":"2018-02-28T03:49:33Z", "INTERVAL":2295.0, "DESCRIPTION":"Updated Existing cards for next week" } ] }, "id":32816.0, "activity_id":22444 });
          alert("a dummy data was pushed");
         }



       sortByName(): void{
          this.people.sort(function(a,b) {
              if(a.user.first_name < b.user.first_name) {
                  return -1;
              }
              if(a.user.first_name > b.user.first_name) {
                                return 1;
                            }
              return 0;
          });
       }



  sortById(): void {
    this.people.sort(function (a, b) {
      if (a.user_id < b.user_id) {
        return -1;
      }
      if (a.user_id > b.user_id) {
        return 1;
      }
      return 0;
    });
  }


myFunction() : void {
      var x = document.getElementById("mySelect").value;

       switch(x) {
               case "SortId":
                   this.people.sort(function (a, b) {
                         if (a.user_id < b.user_id) {
                           return -1;
                         }
                         if (a.user_id > b.user_id) {
                           return 1;
                         }
                         return 0;
                       });
               break;
               case "SortName":
               this.people.sort(function(a,b) {
                             if(a.user.first_name < b.user.first_name) {
                                 return -1;
                             }
                             if(a.user.first_name > b.user.first_name) {
                                               return 1;
                                           }
                             return 0;
                         });
               break;
               case "Push":
               this.people.push({"time_taken":0, "user_id":999, "user":{ "tags":[ "UI Development" ], "img":"https://lh3.googleusercontent.com7rxVanc/photo.jpg?sz=50", "id":50.0, "first_name":"dummy", "last_name":"" }, "date":"2018-02-28T00:00:00+00:00", "description":"Updated Existing cards for next week", "activity":{ "project":{ "client":{ "name":"Pepper Square", "img":"https://gtd.s3.amazonaws.com/client/psq_logo.png", "id":1.0, "archived":false, "data":null }, "img":null, "status":null, "data":{ "admin_group":[ ], "planned_hours":120, "acl":[ 11, 1, 6, 4, 12, 15, 13, 22, 20, 41, 53, 54, 60, 5, 32, 63, 67, 65, 68, 66, 58, 69, 71, 72, 74, 75, 78, 79, 50 ] }, "name":"Internal", "link":null, "client_id":1, "due":"2018-01-23T18:30:00+00:00", "id":52.0, "archived":false }, "title":"Checking trello and GTD", "id":22444.0, "due_on":"2018-02-27T18:30:00+00:00" }, "data":{ "TIME":[ { "STOP":"2018-02-28T04:27:48Z", "START":"2018-02-28T03:49:33Z", "INTERVAL":2295.0, "DESCRIPTION":"Updated Existing cards for next week" } ] }, "id":32816.0, "activity_id":22444 });
                 alert("a dummy data was pushed");
               break;
               default:

           }
    }




}

